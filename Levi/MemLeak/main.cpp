#include <iostream>
#include <thread>

using namespace std;

void memleak() {
    auto * p = new char[1024*1024*1024]();

    cout << p << endl;

    if (p) {
        cout << "Leak..." << endl;
    } else {
        cout << "LEAK FAILURE" << endl;
    }
}

int main() {
    cout << "Starting memory leak!" << endl;

    for (int i = 0; i < 1024; i++) {
        memleak();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    return 0;
}