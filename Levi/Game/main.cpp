#include <iostream>
#include <random>
using namespace std;

int main() {
    int num;
    int guesses = 5;
    float multiplier;
    string difficulty;


    cout << "Select Difficulty: ";
    cin >> difficulty;

    random_device rd;
    mt19937 generator{rd()};

    if (difficulty == "H") {
        multiplier = 2;
        uniform_int_distribution<int> distribution(1, 31);
        num = distribution(generator);
    } else if (difficulty == "M") {
        multiplier = 1.5;
        uniform_int_distribution<int> distribution(1, 16);
        num = distribution(generator);
    } else if (difficulty == "E") {
        multiplier = 1;
        uniform_int_distribution<int> distribution(1, 11);
        num = distribution(generator);
    } else {
        return 1;
    }

    while (guesses > 0) {
        int guess;

        cout << "Enter your guess (" << guesses << " guesses left): ";
        cin >> guess;

        if (guess == num) {
            break;
        } else if (guess > num) {
            cout << "Guess too high!" << endl;
        } else if (guess < num) {
            cout << "Guess too low!" << endl;
        }

        guesses--;
    }

    if (guesses > 0) {
        cout << "You Win!" << endl;
        cout << "Score: " << (guesses * 10 * multiplier) << endl;
    } else {
        cout << "You Lose!" << endl;
        cout << "Answer: " << num << endl;
    }


    return 0;
}