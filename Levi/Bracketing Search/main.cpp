#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int low = 0;
    int high = 100;
    int guess = (high - low) / 2;

    while (true) {
        int input;

        cout << "Guess: " << guess << endl;
        cout << "0 = correct, 1 = too high, 2 = too low" << endl;
        cin >> input;

        if (input == 0) {
            cout << "I win!" << endl;
            return 0;
        } else if (input == 1) {
            high = guess;
            guess -= ceil((high - low) / 2.0);
        } else if (input == 2) {
            low = guess;
            guess += ceil((high - low) / 2.0);

        }

    }
}