//
// Created by Levi Muniz on 3/22/18.
//

#include "MediaLib.h"

int MediaLib::getRating() {
    return rating;
}

void MediaLib::setRating(int r) {
    rating = r;
}

std::string MediaLib::getAuthor() {
    return author;
}

void MediaLib::setAuthor(std::string a) {
    author = std::move(a);
}

std::string MediaLib::getTitle() {
    return title;
}

void MediaLib::setTitle(std::string t) {
    title = std::move(t);
}

