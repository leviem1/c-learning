//
// Created by Levi Muniz on 3/22/18.
//

#ifndef MEDIALIB_SONG_H
#define MEDIALIB_SONG_H

#include "MediaLib.h"

class Song: public MediaLib {
public:
    Song();

    int getDuration();
    void setDuration(int);

private:
    int duration;

};


#endif //MEDIALIB_SONG_H
