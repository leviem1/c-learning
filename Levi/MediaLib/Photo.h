//
// Created by Levi Muniz on 3/22/18.
//

#ifndef MEDIALIB_PHOTO_H
#define MEDIALIB_PHOTO_H


#include "MediaLib.h"

class Photo: public MediaLib {
public:
    Photo();

    int getHeight();
    void setHeight(int);

    int getWidth();
    void setWidth(int);

private:
    int height;
    int width;

};


#endif //MEDIALIB_PHOTO_H
