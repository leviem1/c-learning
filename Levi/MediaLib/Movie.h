//
// Created by Levi Muniz on 3/22/18.
//

#ifndef MEDIALIB_MOVIE_H
#define MEDIALIB_MOVIE_H


#include "MediaLib.h"

class Movie: public MediaLib {
public:
    Movie();

    int getDuration();
    void setDuration(int);

    int getHeight();
    void setHeight(int);

    int getWidth();
    void setWidth(int);

private:
    int duration;
    int height;
    int width;
};


#endif //MEDIALIB_MOVIE_H
