//
// Created by Levi Muniz on 3/22/18.
//

#include "Movie.h"


Movie::Movie() {
    width = 0;
    height = 0;
    duration = 0;
    setRating(0);
    setTitle("");
    setAuthor("");
}

int Movie::getDuration() {
    return duration;
}

void Movie::setDuration(int d) {
    duration = d;
}

int Movie::getHeight() {
    return height;
}

void Movie::setHeight(int h) {
    height = h;
}

int Movie::getWidth() {
    return width;
}

void Movie::setWidth(int w) {
    width = w;
}

