//
// Created by Levi Muniz on 3/22/18.
//

#include <iostream>
#include "Song.h"
#include "Movie.h"
#include "Photo.h"

int main() {
    Song song;

    song.setAuthor("Marian Hill");
    song.setTitle("Down");
    song.setDuration(197);
    song.setRating(5);

    std::cout << song.getTitle() << std::endl;
    std::cout << song.getAuthor() << std::endl;
    std::cout << song.getDuration() << std::endl;
    std::cout << song.getRating() << std::endl;
    std::cout << std::endl;

    Movie movie;

    movie.setAuthor("Levi Muniz");
    movie.setTitle("Little Shack Movie");
    movie.setDuration(36);
    movie.setHeight(720);
    movie.setWidth(1280);
    movie.setRating(3);

    std::cout << movie.getTitle() << std::endl;
    std::cout << movie.getAuthor() << std::endl;
    std::cout << movie.getDuration() << std::endl;
    std::cout << movie.getHeight() << std::endl;
    std::cout << movie.getWidth() << std::endl;
    std::cout << movie.getRating() << std::endl;
    std::cout << std::endl;

    Photo photo;

    photo.setAuthor("Levi Muniz");
    photo.setTitle("Little Shack Photo");
    photo.setHeight(720);
    photo.setWidth(1280);
    photo.setRating(5);

    std::cout << photo.getTitle() << std::endl;
    std::cout << photo.getAuthor() << std::endl;
    std::cout << photo.getHeight() << std::endl;
    std::cout << photo.getWidth() << std::endl;
    std::cout << photo.getRating() << std::endl;
    std::cout << std::endl;

    return 0;
}