//
// Created by Levi Muniz on 3/22/18.
//

#include <string>

#ifndef MEDIALIB_MEDIALIB_H
#define MEDIALIB_MEDIALIB_H

class MediaLib {
public:
    int getRating();
    void setRating(int);

    std::string getAuthor();
    void setAuthor(std::string);

    std::string getTitle();
    void setTitle(std::string);

protected:
    std::string author;
    std::string title;
    int rating;
};

#endif //MEDIALIB_MEDIALIB_H
