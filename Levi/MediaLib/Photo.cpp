//
// Created by Levi Muniz on 3/22/18.
//

#include "Photo.h"

Photo::Photo() {
    width = 0;
    height = 0;
    setRating(0);
    setTitle("");
    setAuthor("");
}

int Photo::getHeight() {
    return height;
}

void Photo::setHeight(int h) {
    height = h;
}

int Photo::getWidth() {
    return width;
}

void Photo::setWidth(int w) {
    width = w;
}
