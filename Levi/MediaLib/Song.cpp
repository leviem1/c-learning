//
// Created by Levi Muniz on 3/22/18.
//

#include "Song.h"

Song::Song() {
    duration = 0;
    setRating(0);
    setTitle("");
    setAuthor("");
}

int Song::getDuration() {
    return duration;
}

void Song::setDuration(int d) {
    duration = d;
}