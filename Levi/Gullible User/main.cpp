#include <iostream>

using namespace std;

int main() {
    int input;

    for (int i = 0; i < 10; i++) {
        cout << "Please enter any number other than " << i << endl;
        cin >> input;

        if (input == i) {
            cout << "Hey! You weren't supposed to enter " << i << "!" << endl;
            return 0;
        }
    }

    cout << "Wow, you're more patient than I am, you win." << endl;

    return 0;
}