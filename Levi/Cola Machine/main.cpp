#include <iostream>

using namespace std;

void vend(float choicePrice, float money) {
    int q = 0, d = 0, n = 0, p = 0;
    float change = money - choicePrice;

    cout << "VENDING!" << endl;
    cout << "CHANGE: $" << change << endl;

    q = change / .25;
    change -= q * .25;
    d = change / .1;
    change -= d * .1;
    n = change / .05;
    change -= n * .05;
    p = change / .01;

    cout << q << " quarter(s)" << endl;
    cout << d << " dime(s)" << endl;
    cout << n << " nickel(s)" << endl;
    cout << p << " penny(s)" << endl;
}

int main() {
    int choice;
    float money = 0;

    while (true) {
        cout << "1 COKE" << endl;
        cout << "2 PEPSI" << endl;
        cout << "3 MOUNTAIN DEW" << endl;
        cout << "4 GATORAID" << endl;
        cout << "5 DR. PEPPER" << endl;
        cout << "6 INPUT MONEY" << endl;
        cin >> choice;

        if (choice == 6) {
            float input;
            cout << "AMOUNT: $";
            cin >> input;
            money += input;
            continue;
        }

        float choicePrice;
        switch (choice) {
            case 1:
                choicePrice = 1.75;
                break;
            case 2:
                choicePrice = 1.50;
                break;
            case 3:
                choicePrice = 2.00;
                break;
            case 4:
                choicePrice = 1.00;
                break;
            case 5:
                choicePrice = 1.25;
                break;
            default:
                cout << "INVALID CHOICE!" << endl;
                continue;
        }

        if (money == 0) {
            cout << "PRICE: $" << choicePrice << endl;
        } else {
            if (money < choicePrice) {
                cout << "INSUFFICIENT FUNDS!" << endl;
                cout << "PRICE: $" << choicePrice << endl;
                cout << "AMOUNT: $" << money << endl;
                continue;
            }
            vend(choicePrice, money);
            break;
        }
    }

    return 0;
}